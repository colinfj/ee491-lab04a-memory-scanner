///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - WordCount
///
/// @file argParser.C
/// @version 1.0
/// @see https://linux.die.net/man/3/getopt for documentation on getopt_long() uses
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 04 - MemoryScanner - EE 491F - Spr 2021
/// @date   11_002_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "parseMaps.h"

void parseMaps(FILE *);
char nextLine(FILE *);

int main(int argc, char *argv[])
{
   FILE *filePtr;

   filePtr = fopen("/proc/self/maps", "r");

   if(filePtr == NULL)
   {
      fprintf(stderr, "Could not open %s\n", MAPSFILE);
      exit(EXIT_FAILURE);
   }

   parseMaps(filePtr);
   
   exit(EXIT_SUCCESS);
}


void parseMaps(FILE *filePtr)
{
   struct Addresses addresses;
   char strBuffer[35], permissions[5];
   bool isEnd = false;
   int numOfA = 0,
       bytesRead = 0,
       index = 0;

   fscanf(filePtr, "%s", strBuffer);

   while(!isEnd)
   {
      fgetc(filePtr);
      fscanf(filePtr, "%s", permissions);

      //find the length of the address string 
      while(strBuffer[index] != '\0')
      {
         index++;
      }

      if(permissions[0] == 'r' && index <= SIX_BYTE_ADDRESS)
      {
         sscanf(strBuffer, "%p%*c%p", &addresses.start, &addresses.end);

         while(addresses.start < addresses.end)
         {
            bytesRead++;

            if(*( (char*) addresses.start)  == 0x41)
            {
               numOfA++;
            }

            addresses.start++;
         }

         printf("%s  %s", strBuffer, permissions);
         printf(" | read [%d] bytes. %d [A]'s in the file\n", bytesRead, numOfA);
         numOfA = 0;
         bytesRead = 0;
      }
      else
      {
         printf("%s  %s", strBuffer, permissions);
         printf(" | Unable to read\n");
      }

      if(nextLine(filePtr) == EOF)
      {
         isEnd = true;
      }
      else
      {
         fscanf(filePtr, "%s", strBuffer);
         index = 0;
      }
   }
   
   fclose(filePtr);

}


char nextLine(FILE * filePtr)
{
   char lastChar;

   lastChar = fgetc(filePtr);

   while(lastChar != '\n' && lastChar != EOF)
   {
      lastChar = fgetc(filePtr);
   }
   
   return lastChar;
}
