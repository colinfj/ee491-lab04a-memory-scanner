#ifndef PARSEMAPS_H
#define PARSEMAPS_H

#define MAPSFILE "/proc/self/maps"
#define SIX_BYTE_ADDRESS 25

struct Addresses {
   void *start;
   void *end;
};

#endif
