# Build a Hello World C program

CC     = gcc
CFLAGS = -g -Wall

TARGET = parseMaps

all: $(TARGET)

parseMaps: parseMaps.c
	$(CC) $(CFLAGS) -o $(TARGET) parseMaps.c

clean:
	rm $(TARGET)

test:
	./parseMaps

